# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/users/r/d/rdotey/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/users/r/d/rdotey/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/users/r/d/rdotey/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/users/r/d/rdotey/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


# >>> nvm initialize >>>
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# <<< nvm initialize <<<




# User specific aliases and functions
echo "
##########################       Welcome $(whoami)       ##########################

The ~/.bashrc will load user specific bash commands used to manage the Storywrangling system.

Loading system config for Storywrangling.org ...
"


# Variables used in this file
PORT_UI_DEV="8051"
PORT_UI_PROD="8050"
PORT_API_DEV="3001"
PORT_API_PROD="3000"

CONDA_ENV="storywrangling"

DIR_SRV="/srv"
DIR_DB="/srv/storywrangling"
DIR_API="/srv/storywrangling-website"
DIR_UI="/srv/storywrangling-website/story-viewer"
DIR_LOGS="/srv/storywrangling-website/debug/logs"

echo "##  Conda Environment Name: $CONDA_ENV
##
##  +----------+--------------+------------+
##  | Ports    | Development  | Production |
##  | Web API  | $PORT_API_DEV         | $PORT_API_PROD       |
##  | Website  | $PORT_UI_DEV         | $PORT_UI_PROD       |
##  +----------+--------------+------------+
##
##  DB package directory: $DIR_DB
##  API directory:        $DIR_API
##  Website directory:    $DIR_UI
##  Log directory:        $DIR_LOGS
"


echo "
Adding bash commands:"


# Directory navigaton shortcuts cd
echo "
 - Navigaton Shortcuts"
echo "   [to-db]         change directory to the storywrangling db connection"
alias to-db="cd $DIR_DB"

echo "   [to-api]        change directory to the api used by the storywrangling.org website"
alias to-api="cd $DIR_API"

echo "   [to-ui]         change directory to the storywrangling.org website"
alias to-ui="cd $DIR_UI"

echo "   [to-logs]       change directory to the log folder"
alias to-logs="cd $DIR_LOGS"


# Enviroment
echo "
 - Enviroment"
echo "   [env-start]     activate the storywrangling conda enviroment"
alias env-start="cd $DIR_SRV && conda activate storywrangling"

echo "   [env-stop]      deactivate the storywrangling conda enviroment"
alias env-stop="conda deactivate"


echo "
 - DB Connection Controls"
echo "   [find-db]       list running python packages that contain 'story' (only one version of the db package can run at a time)"
alias find-db="pip list | grep story"

echo "   [run-db]        start the storywrangling python package used for database communication"
alias run-db="python $DIR_DB/setup.py"

echo "   [run-db-dev]    start the storywrangling python package in dev mode"
alias run-db-dev="python $DIR_DB/setup.py develop"

echo "   [end-db]        stop the storywrangling python process"
alias end-db="pip uninstall storywrangling"


echo "
 - Website API Controls"
echo "   [find-api] list id of process running on port $PORT_API_PROD"
alias find-api-prod="lsof -t -i:$PORT_API_PROD"
alias find-api="find-api-prod"

echo "   [find-api-dev]  list id of process running on port $PORT_API_DEV"
alias find-api-dev="lsof -t -i:$PORT_API_DEV"

echo "   [run-api]  nohup uwsgi serve the storywrangling.org website"
alias run-api-prod="to-api && nohup uwsgi --http :$PORT_API_PROD --wsgi-file $DIR_API/prod/api/uwsgi.py > $DIR_LOGS/prod-api.txt & disown"
alias run-api="run-api-prod"

echo "   [run-api-dev]   nohup uwsgi serve the storywrangling.org DEV website"
alias run-api-dev="to-api && nohup uwsgi --http :$PORT_API_DEV --wsgi-file $DIR_API/dev/api/uwsgi.py > $DIR_LOGS/dev-api.txt & disown"



# alias dev-api="rm -f dev/api/logs/*.csv && . activate test_env && touch dev/api/logs/querylog.csv && touch dev/api/logs/responselog.csv && python3 dev/api/init_logs.py && nohup uwsgi --http :3000 --wsgi-file dev/api/uwsgi.py > debug/dev-api.txt & disown"
#
# alias prod-api="rm -f prod/api/logs/*.csv && . activate test_env && touch prod/api/logs/querylog.csv && touch prod/api/logs/responselog.csv && python3 prod/api/init_logs.py && nohup uwsgi --http :3001 --wsgi-file prod/api/uwsgi.py > debug/prod-api.txt & disown"



echo "
 - Website Controls"
echo "   [find-ui]  list id of process running on port $PORT_UI_PROD"
alias find-ui-prod="lsof -t -i:$PORT_UI_PROD"
alias find-ui="find-ui-prod"

echo "   [find-ui-dev]   list id of process running on port $PORT_UI_DEV"
alias find-ui-dev="lsof -t -i:$PORT_UI_DEV"

echo "   [build-ui]      run yarn build on the reactjs website in /src"
alias build-ui="to-ui && yarn build"

echo "   [run-ui]   nohup serve the storywrangling.org website"
alias run-ui-prod="to-ui && nohup npx serve -s -n $DIR_UI/build -l $PORT_UI_PROD > $DIR_LOGS/prod-ui.txt & disown"
alias run-ui="run-ui-prod"

echo "   [run-ui-dev]    nohup serve the storywrangling.org DEV website"
alias run-ui-dev="to-ui && nohup npx serve -s -n $DIR_UI/build -l $PORT_UI_DEV > $DIR_LOGS/dev-ui.txt & disown"

# alias dev-ui="nohup serve -s -n build -l 8051 > ~/debug/dev-ui.txt & disown"
# alias prod-ui="nohup serve -s -n build -l 8050 > ~/debug/prod-ui.txt & disown"

echo "
 - Bashrc Shortcut"
echo "   [edit-bash]     opens ~/.bashrc in vim"
alias edit-bash="vim ~/.bashrc"

echo "   [refresh-bash]  refreshes the changed .bashrc in the system"
alias refresh-bash="source ~/.bashrc"


echo "   NOTE: You are now running the storywrangling conda environment"
env-start

echo -e "\n\n########################## bash command loading complete ##########################\n\n"