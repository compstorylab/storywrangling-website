[< Menu](index.md)

# SSH Connection Config

OpenSSH allows you to set up a per-user configuration file where you can store different SSH options for each remote machine you connect to. This file is named `config`, and it is stored in the `.ssh` folder under the user’s home directory.

**Location on Mac**

    ~/Users/USERNAME/.ssh/config

**Location on Windows**

    C:\Users\USERNAME\.ssh\config

### Initialize SSH location
 
Your `.ssh` directory is automatically created when you use the ssh command for the first time. If you have never used ssh before under this user account please create a `.ssh` folder inside your user directory. 

**Create a SSH config on Mac**

Then, if one doesn't already exist, create a directory named `.ssh`, inside your user directory.  

    $ mkdir -p ~/.ssh 
    $ chmod 0700 ~/.ssh

Then, create a file named `config`, with no file extension, inside your `.ssh` folder.

    $ touch ~/.ssh/config

This file must be readable and writable only by the user and not accessible by others.

    $ chmod 600 ~/.ssh/config

For further details see: [How to access and modify a SSH file on mac?](https://stackoverflow.com/questions/54133300/how-to-access-and-modify-a-ssh-file-on-mac)

**Create a SSH config on  Windows**


Creating the `config` file can be done in analog through the Windows file explorer.

For further details see: [Location of OpenSSH configuration file on Windows](https://superuser.com/questions/1537763/location-of-openssh-configuration-file-on-windows)


### SSH client configuration file

The `config` file can be edited in a plain text editor.

```
IgnoreUnknown AddKeysToAgent,UseKeychain

Host vacc
    HostName vacc-user1.uvm.edu
    User USERNAME

Host storywrangling
    ProxyCommand ssh -q vacc nc storywrangling.uvm.edu 22
    User USERNAME

Host *
    AddKeysToAgent yes
    UseKeychain yes
    IdentityFile ~/.ssh/id_ed25519
```

[Full SSH Config Documentation](https://man.openbsd.org/ssh_config)

To test the config open a new terminal or command prompt and type:

    $ ssh storywrangling

You will then be asked for your [UVM credentials](https://wiki.uvm.edu/w/Network_ID) password twice, once for the `vacc` server and once for the `storywrangling` server.

    $ USERNAME@vacc-user1.uvm.edu's password:
    $ USERNAME@storywrangling's password:

*This will not work for off campus users without the [Cisco AnyConnect VPN Client](doc_connect_vpn.md).*