[< Menu](index.md)

## Install Node.js server

Project dependencies and build scripts are run with [Node Version Manager](https://github.com/nvm-sh/nvm#installing-and-updating)

    $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    $ nvm --version

To download, compile, and install the latest release of node, do this:

    nvm install node # "node" is an alias for the latest version

To install a specific version of node:

    nvm install 14.7.0 # or 16.3.0, 12.22.1, etc

You can list available versions using ls-remote:

    nvm ls-remote


https://github.com/nodesource/distributions

    $ nvm install 16
    $ node --version
    $ npm --version


## Install Yarn Package Manager

https://yarnpkg.com/getting-started/install

The preferred way to manage Yarn is through Corepack, a new binary shipped with all Node.js releases starting from 16.10. It acts as an intermediary between you and Yarn, and lets you use different package manager versions across multiple projects without having to check-in the Yarn binary anymore.

###### Node.js >=16.10

Corepack is included by default with all Node.js installs, but is currently opt-in. To enable it, run the following command:

    $ corepack --version
    $ corepack enable
    $ yarn --version
