[< Menu](index.md)

## Git Version Control

This project is stored in a Git repository, [gitlab.com/compstorylab/storywrangling-website](https://gitlab.com/compstorylab/storywrangling-website.git), and requires an additional repository, [gitlab.com/compstorylab/storywrangling](https://gitlab.com/compstorylab/storywrangling.git), to run. 

#### Install Git on Linux

Yum has the latest Git package available:

    $ sudo yum install git
    $ git --version

Or try apt-get:

    $ sudo apt-get update
    $ sudo apt-get install git
    $ git --version

https://www.atlassian.com/git/tutorials/install-git#linux

#### Install Git on MacOS

Most versions of MacOS will already have Git installed, and you can activate it through the terminal with:

    $ git version

However, if you don't have Git, you can install the latest version by Homebrew:

    $ brew install git

Or by MacPorts:

    $ sudo port install git

Further information is available on the[ Git MacOS Download](https://git-scm.com/download/mac/) page. Alternatively [GitHub Desktop](https://desktop.github.com/) provides a easy to use Git interface for this OS. 

#### Install Git on Windows

To install Git on Windows OS navigate to [Git for Windows installer](https://git-scm.com/download/win) and download the latest version. 
Alternatively [GitHub Desktop](https://desktop.github.com/) provides a easy to use Git interface for this OS. 

## Generate a SSH key

This creates a new SSH key, using the provided email as a label.

```
$ ssh-keygen -t ed25519 -C "your_email@example.com"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/c/Users/USERNAME/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /c/Users/USERNAME/.ssh/id_ed25519
Your public key has been saved in /c/Users/USERNAME/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:Znn+JwNPFl1+ZAa0mcUgJQP4wqzV4Qb64zSVeVOByAw your_email@example.com
The key's randomart image is:
+--[ED25519 256]--+
|         +E+++*+.|
|        + + .+ **|
|         .   .+*.|
|         .o .o..o|
|      . So.o++o .|
|       o.oo.O. . |
|         .+X .   |
|         .+o= .  |
|         ....+   |
+----[SHA256]-----+

```

#### Adding your SSH key to the ssh-agent

```
$ ssh-add ~/.ssh/id_ed25519
Identity added: /c/Users/USERNAME/.ssh/id_ed25519 (your_email@example.com)
```

Further Documentation: [Generating a new SSH key and adding it to the ssh-agent](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

## Setup SSH with GitLab

You will need to add your [SSH Public Key to your GitLab account](https://docs.gitlab.com/ee/user/ssh.html) unless you used [GitHub Desktop](https://desktop.github.com/) to connect to the repo.