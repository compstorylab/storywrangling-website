[< Menu](index.md)

## Clone Repositories

    $ cd /srv
    $ git clone https://gitlab.com/compstorylab/storywrangling.git
    $ git clone https://gitlab.com/compstorylab/storywrangling-website.git
    




## Set your Git Lab credentials in the config

    git config --global user.name "USER NAME"
    git config --global user.email "EMAIL@uvm.edu"


## Install Website Dependencies

    
    $ cd /srv/storywrangling-website/story-viewer
    $ yarn install
    $ yarn global add serve



    $ serve -s build