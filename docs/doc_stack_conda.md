[< Menu](index.md)

# Anaconda Installation

### Install Anaconda on Mac OS or Windows

Anaconda can be installed with pre built packages available here: https://www.anaconda.com/products/distribution

### Install Anaconda on Linux

To install Anaconda via command line first find the URL to the desired version here (look at the bottom for "Anaconda Installers"): https://www.anaconda.com/products/distribution 

Then use `curl` to download, and `bash` to run the package.

    $ curl -0 https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-x86_64.sh
    $ bash Anaconda3-2021.11-Linux-x86_64.sh

Confirm `conda` has been installed before removing the downloaded file.

    $ conda --version
    conda 4.13.0
    $ python --version
    Python 3.10.4
    $ rm Anaconda3-2022.05-Linux-x86_64.sh

Further install documentation: https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html

# Conda Enviroment

Create a conda environment named `storywrangling`.

    $ conda create --name storywrangling
    $ conda activate storywrangling
    
Add channels to download packages.

    $ conda config --add channels conda-forge
    $ conda config --add channels anaconda
    $ conda config --set channel_priority strict
    
    $ conda install nginx
    $ nginx -v

    $ conda install pip
    $ pip install datetime flask flask-cors numpy os-sys pandas pickle5 urllib3 uuid uwsgi
    $ pip list

pip install os-sys
pip install flask
pip install flask-cors
pip install numpy
pip install pandas
pip install urllib3
pip install uuid

pip install os-sys
pip install pickle5
pip install uwsgi


The uWSGI project aims at developing a full stack for building hosting services. Application servers (for various programming languages and protocols), proxies, process managers and monitors are all implemented.
Install uWSGI in conda: https://anaconda.org/conda-forge/uwsgi

// Other conda commands
https://docs.conda.io/projects/conda/en/4.6.0/user-guide/tasks/manage-environments.html#

conda env list
conda remove --name myenv --all


pip list

Nginx is an HTTP and reverse proxy server
Install nginx in conda: https://github.com/conda-forge/nginx-feedstock

    $ conda config --add channels conda-forge
    $ conda config --add channels anaconda
    $ conda config --set channel_priority strict
    $ conda install nginx
    $ nginx -v
    
    $ systemctl status nginx



Update the repository:

    $ sudo yum update

Install NGINX Open Source:

    $ sudo yum install nginx

Verify the installation:

    $ nginx -v
    
    $ systemctl status nginx


# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
datetime
flask
flask-cors
numpy
pandas
os-sys
pickle5
urllib3
uuid
uwsgi

# Storywrangling requirements
# https://gitlab.com/compstorylab/storywrangling/-/blob/master/requirements.txt
matplotlib
pymongo
tqdm
ujson


## Running

### Storywrangling

    $ cd storywrangling
    $ python setup.py install
    $ pip list
    Package            Version   Location
    ------------------ --------- -------------------
    certifi            2021.10.8
    click              8.1.2
    cycler             0.11.0
    DateTime           4.4
    Flask              2.1.1
    fonttools          4.31.2
    importlib-metadata 4.11.3
    itsdangerous       2.1.2
    Jinja2             3.1.1
    kiwisolver         1.4.2
    MarkupSafe         2.1.1
    matplotlib         3.5.1
    numpy              1.22.3
    packaging          21.3
    pandas             1.4.2
    pickle5            0.0.11
    Pillow             9.1.0
    pip                21.2.4
    pymongo            4.1.0
    pyparsing          3.0.7
    python-dateutil    2.8.2
    pytz               2022.1
    setuptools         58.0.4
    six                1.16.0
    storywrangling     0.2.6     /srv/storywrangling
    tqdm               4.64.0
    ujson              5.2.0
    urllib3            1.26.9
    uuid               1.30
    uWSGI              2.0.19.1
    Werkzeug           2.1.1
    wheel              0.37.1
    zipp               3.8.0
    zope.interface     5.4.0
