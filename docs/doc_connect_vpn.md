[< Menu](index.md)

## Cisco AnyConnect

The University of Vermont's Virtual Private Network service allows remote users with [UVM credentials](https://wiki.uvm.edu/w/Network_ID) to securely access resources that are normally only available to on-campus users.

The Cisco AnyConnect Client is a Virtual Private Network client for providing secure access to the above, UVM VPN resources from off campus. 

[Install the Cisco AnyConnect VPN client](https://www.uvm.edu/it/kb/article/install-cisco-vpn/)

**Additionally, you must set up a Multi-Factor Authentication device to access the UVM’s VPN.** [See here for information and guides on available MFA methods](https://www.uvm.edu/it/kb/article/duo-multi-factor-authentication).

### Duo login process

Open the Cisco AnyConnect VPN Client. Under "Ready to Connect", enter either `sslvpn2.uvm.edu` then click "Connect".

![AnyConnect VPN Client!](/docs/images/AnyConnect-SSLVPN2-768x380.png)

Ensure your NetID and NetID Password are entered in the corresponding fields.

![AnyConnect VPN Client!](/docs/images/ciscoDuoLogin.png)

The **Password** field is where you will specify a Multi-Factor Authentication method. A common option is **SMS Text Message**, and you would enter `sms` into the Password field.

For more Multi-Factor Authentication methods see https://www.uvm.edu/it/kb/article/sslvpn2/

### SMS Text Message Dual Authentication

1. Type "sms" in the Password field.

1. Click "OK".

1. You will receive a "Login failed" error, but you should will receive SMS passcodes on your phone. If you do not receive codes, try entering "sms" again, or use a different MFA method.
![AnyConnect VPN Client!](/docs/images/ciscoText-1.png)

1. Enter one of the SMS passcodes into the Password field.

1. Ensure your NetID and NetID Password are properly entered, then click OK.

Complete instructions: https://www.uvm.edu/it/kb/article/sslvpn2/