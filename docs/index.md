## Documentation


#### Connecting to UVM servers

Available only to users with [UVM credentials](https://wiki.uvm.edu/w/Network_ID).

1. [Cisco AnyConnect VPN Client](doc_connect_vpn.md): Allows remote users with [UVM credentials](https://wiki.uvm.edu/w/Network_ID) to securely access resources that are normally only available to on-campus users.
2. [SSH Configuration](doc_connect_ssh.md): Set SSH connection alias to UVM servers.


#### Server setup

1. Install [Git Version Control](doc_stack_git.md) and setup a SSH key.
2. Install [Node.js](doc_stack_nodejs.md) and [Yarn Package Manager](doc_stack_yarn.md)
4. [Conda / Python](doc_stack_conda.md)


#### Install project requirements

Clone [project repositories](doc_project_repo.md) and install required packages.

#### Build and run Storywrangling

1. [Project Build Commands](doc_admin_bashrc.md)
2. [Running Storywrangling.org](doc_run_storywrangling.md)